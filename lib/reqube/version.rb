module Reqube
  
  VERSION_INFO = [0, 0, 1, 'dev'] unless defined?(self::VERSION_INFO)
  VERSION = VERSION_INFO.map(&:to_s).join('.') unless defined?(self::VERSION)

  def self.version
    VERSION
  end  

end
